﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UsersInfo {
    public int[] HashCode { get; set; }
    public string[] NickName { get; set; }
    public int[] Weapon { get; set; }
    public int PlayerCount { get; private set; }

    private UsersInfo() { }
    public UsersInfo(int count) {
        PlayerCount = count;
        HashCode = new int[count];
        NickName = new string[count];
        Weapon = new int[count];
        for (int i = 0; i < count; ++i) {
            HashCode[i] = new int();
            NickName[i] = "";
            Weapon[i] = new int();
        }
    }
}
