﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LogView : MonoBehaviour {
    public static string _Logs = "";
    private string _OldLogs = "";
    private ScrollRect _ScrollRect;
    private Text _TextLog;

	// Use this for initialization
	void Start() {
        StopAllCoroutines();
        _ScrollRect = this.gameObject.GetComponent<ScrollRect>();
        _TextLog = _ScrollRect.content.GetComponentInChildren<Text>();
	}
	
	// Update is called once per frame
	void Update() {
        if (_ScrollRect != null && _Logs != _OldLogs) {
            _TextLog.text = _Logs;
            StartCoroutine(DelayMethod(5, () =>{
                _ScrollRect.verticalNormalizedPosition = 0;
            }));
            _OldLogs = _Logs;
        }
	}
    public static void Clear() {
        _Logs = "";
    }
    public static void Log(string logText) {
        _Logs += (DateTime.Now.ToString("hh:mm:ss ") + logText + "\n");
        Debug.Log(logText);
    }

    private IEnumerator DelayMethod(int delayCount, Action action) {
        for (var i = 0; i < delayCount; ++i) {
            yield return null;
        }
        action();
    }
    public void StopLogView() {
        StopCoroutine("DelayMethod");
    }
}
