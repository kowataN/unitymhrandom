﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputEvents : MonoBehaviour
{
    [SerializeField] private InputField _InputFieid = default;

    public void OnValueChanged()
    {
        string val = _InputFieid.text;
        if (val.IndexOf("\n") != -1)
        {
            _InputFieid.text = val;
        }
    }
}
