﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Photon;
using UnityEngine.UI;
using TrueSync;
using UnityEngine.SceneManagement;
using System.Linq;

// Connects to Photon Cloud, manages GUI to create/join game rooms.
public class Main : PunBehaviour {
    private const int kMaxPlayerCount = 4;

    private enum PanelType { Login, RoomList, Room };
    private string lobbyName = "MHWeaponRandom";

    [Header("Login Panel")]
    [SerializeField] private GameObject _LoginPanel = default;
    [SerializeField] private InputField _UserNameInput = default;

    [Header("RoomList Panel")]
    [SerializeField] private GameObject _RoomListPanel = default;
    [SerializeField] private Text _ListCountText = default;
    [SerializeField] private RectTransform _RoomListContent = default;
    [SerializeField] private GameObject _RoomPrefabBtn = default;
    [SerializeField] private InputField _RoomNameInput = default;
    [SerializeField] private Text _TextUserName = default;

    [Header("Room Panel")]
    [SerializeField] private GameObject _RoomPanel = default;
    [SerializeField] private GameObject[] playerBoxes = default;
    [SerializeField] private Button startRouletteBtn = default;
    [SerializeField] private Toggle suffToggle = default;
    [SerializeField] private Text _RoomNameText = default;
    [SerializeField] private Text _RouleteText = default;

    private string _UserName;
    private string _RoomName;

    /// true:ルーレット中、false:待機
    private bool IsStart { get; set; }

    public static Main _Instance;

    private string[] _Weapons = {
        "大剣","太刀","片手剣","双剣","ハンマー","狩猟笛","ランス","ガンランス",
        "スラッシュアックス","チャージアックス","操虫棍","ライトボウガン","ヘビィボウガン","弓"
    };

    /// ローカルユーザー情報
    private UsersInfo _UsersInfo = new UsersInfo(kMaxPlayerCount);

    // Connects to photon
    void Start() {
        // 向き設定
        Screen.orientation = ScreenOrientation.LandscapeLeft;
        Screen.autorotateToLandscapeLeft = true;
        Screen.autorotateToLandscapeRight = true;

        _Instance = this;
        PhotonNetwork.CrcCheckEnabled = true;
        suffToggle.isOn = true;
        if (PhotonNetwork.connected) {
            OnReceivedRoomListUpdate();
            ActivePanel(PanelType.RoomList);
            return;
        }
        string isUserName = PlayerPrefs.GetString("KEY_USERNAME");
        Debug.Log("name : " + isUserName);
        if (isUserName.Length == 0)
        {
            ActivePanel(PanelType.Login);
        }
        else
        {
            _UserNameInput.text = isUserName;
            LoginPanel_OkBtn();
        }

        // スリープさせない
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
    }

    void Update() {
        // ランダム武器表示
        ShowWeaponAtRandom();
    }

    void ShowWeaponAtRandom() {
        if (IsStart == false) {
            return;
        }
        // 武器ランダム表示※実際のユーザー数分ループ
        for (int index = 0; index < PhotonNetwork.playerList.Length; index++) {
            Transform playerBox = playerBoxes[index].transform;
            playerBox.gameObject.SetActive(true);
            Text weaponNameText = playerBox.Find("weaponNameText").GetComponent<Text>();
            weaponNameText.text = _Weapons[Random.Range(0, _Weapons.Length - 1)];
        }
    }

    // aftert the user put his username and hit ok the main planel is shown
    public void LoginPanel_OkBtn() {
        this._UserName = _UserNameInput.text;
        _TextUserName.text = "ユーザー名：" + this._UserName;
        PlayerPrefs.SetString("KEY_USERNAME", this._UserName);
        this._LoginPanel.SetActive(false);

        PhotonNetwork.player.NickName = this._UserName;
        PhotonNetwork.lobby = new TypedLobby(lobbyName, LobbyType.Default);
        PhotonNetwork.ConnectUsingSettings("v1.0");

        ActivePanel(PanelType.RoomList);
    }

    // go back from a match menu to main menu
    public void RoomListPanel_BackBtn() {
        PhotonNetwork.Disconnect();
        ActivePanel(PanelType.Login);
    }

    public void RoomListPanel_JoinBtn() {
        this._RoomName = _RoomNameInput.text.Length == 0 ? this._UserName : _RoomNameInput.text;
        RoomOptions roomOptions = new RoomOptions();
        roomOptions.IsVisible = true;
        roomOptions.MaxPlayers = 4;
        PhotonNetwork.JoinOrCreateRoom(this._RoomName, roomOptions, TypedLobby.Default);
    }

    public void RoomPanel_BackBtn() {
        PhotonNetwork.LeaveRoom();
        ActivePanel(PanelType.RoomList);
    }

    public override void OnConnectionFail(DisconnectCause cause) {
        ActivePanel(PanelType.Login);
    }

    public override void OnConnectedToMaster() {
        PhotonNetwork.JoinLobby(PhotonNetwork.lobby);
    }

    // updates the possible matches list
    public override void OnReceivedRoomListUpdate() {
        RoomInfo[] roomList = PhotonNetwork.GetRoomList();
        _ListCountText.text = "部屋一覧 : " + roomList.Length;

        int currentMatchesCount = this._RoomListContent.transform.childCount;
        if (roomList.Length >= currentMatchesCount) {
            for (int index = 0; index < (roomList.Length - currentMatchesCount); index++) {
                GameObject newMatchBtn = Instantiate(_RoomPrefabBtn);
                if (newMatchBtn != null) {
                    newMatchBtn.transform.SetParent(this._RoomListContent, false);
                }
            }
        } else {
            for (int index = 0; index < (currentMatchesCount - roomList.Length); index++) {
                Destroy(this._RoomListContent.transform.GetChild(currentMatchesCount - (index + 1)).gameObject);
            }
        }
        for (int index = 0; index < roomList.Length; ++index) {
            MatchingJoiner matchingJoiner = this._RoomListContent.transform.GetChild(index).GetComponent<MatchingJoiner>();
            if (matchingJoiner == null) {
                continue;
            } 
            matchingJoiner.UpdateRoom(roomList[index]);

            RectTransform matchJointRect = matchingJoiner.GetComponent<RectTransform>();
            matchJointRect.localPosition = new Vector3(matchJointRect.localPosition.x, -(index * matchJointRect.sizeDelta.y), 0);
        }

        this._RoomListContent.sizeDelta = new Vector2(this._RoomListContent.sizeDelta.x,
                                                      roomList.Length * _RoomPrefabBtn.GetComponent<RectTransform>().sizeDelta.y);
    }
    public override void OnJoinedLobby() {
        ActivePanel(PanelType.RoomList);
    }
    public override void OnJoinedRoom() {
        this._RoomName = PhotonNetwork.room.Name;
        this._RoomNameText.text = "部屋【" + this._RoomName + "】";

        PlayerUpdateVisibility();
        ActivePanel(PanelType.Room);

        UpdatePlayerList();
    }
    public override void OnPhotonPlayerConnected(PhotonPlayer newPlayer) {
        UpdatePlayerList();
    }
    public override void OnMasterClientSwitched(PhotonPlayer newMasterClient) {
        PlayerUpdateVisibility();
    }
    public override void OnPhotonPlayerDisconnected(PhotonPlayer disconnetedPlayer) {
        UpdatePlayerList();
    }

    private void PlayerUpdateVisibility() {
        //startRouletteBtn.gameObject.SetActive(PhotonNetwork.isMasterClient);
        startRouletteBtn.interactable = PhotonNetwork.isMasterClient;
        suffToggle.interactable = PhotonNetwork.isMasterClient;
        _RouleteText.color = PhotonNetwork.isMasterClient ? Color.white : Color.gray;
    }

    public void UpdatePlayerList() {
        ClearPlayersGUI();
        // ホストがソートしてゲストにユーザーリストを渡す
        if (PhotonNetwork.isMasterClient) {
            // ハッシュコードでソートしてローカルデータを更新する
            UpdateLocalUserInfo(SortPlayerlist());
            ShowPlayerList(PhotonNetwork.playerList.Length, _UsersInfo.HashCode, _UsersInfo.NickName);
            photonView.RPC("ShowPlayerList", PhotonTargets.Others, PhotonNetwork.playerList.Length, 
                           _UsersInfo.HashCode, _UsersInfo.NickName);
        }
    }
    private PhotonPlayer[] SortPlayerlist() {
        // ハッシュコードでソート
        var query = PhotonNetwork.playerList.OrderBy(n => n.GetHashCode());
        return query.ToArray();
    }
    private void UpdateLocalUserInfo(PhotonPlayer[] playerList) {
        for (int i =0; i < playerList.Length;  ++i) {
            _UsersInfo.HashCode[i] = playerList[i].GetHashCode();
            _UsersInfo.NickName[i] = playerList[i].NickName;
        }
    }

    private void ClearPlayersGUI() {
        foreach (GameObject playerBox in playerBoxes) {
            playerBox.SetActive(false);
            playerBox.transform.Find("PlayerNameText").GetComponent<Text>().text = "";
        }
    }

    private void ActivePanel(PanelType panelType) {
        this._LoginPanel.SetActive(panelType == PanelType.Login ? true : false);
        this._RoomListPanel.SetActive(panelType == PanelType.RoomList ? true : false);
        this._RoomPanel.SetActive(panelType == PanelType.Room ? true : false);
    }

    public void StartRouletteBtn() {
        if (IsStart == false) {
            photonView.RPC("StartRoulette", PhotonTargets.All, true);
        } else {
            int[] setWeapon = new int[4];
            if (suffToggle.isOn == true) {
                var suffList = new List<int>();
                while (true) {
                    suffList.Clear();
                    for (int i = 0; i < setWeapon.Length; ++i) {
                        setWeapon[i] = Random.Range(0, _Weapons.Length - 1);
                    }
                    foreach (int value in setWeapon) {
                        if (suffList.Contains(value) == false) {
                            suffList.Add(value);
                        }
                    }
                    if (suffList.Count == 4) {
                        break;
                    }
                }
            } else {
                for (int i = 0; i < setWeapon.Length; ++i) {
                    setWeapon[i] = Random.Range(0, _Weapons.Length - 1);
                }
            }
            photonView.RPC("SyncWeapon", PhotonTargets.All, setWeapon, false);
        }
    }

    [PunRPC]
    public void StartRoulette(bool isStart) {
        IsStart = isStart;
    }

    [PunRPC]
    public void SyncWeapon(int[] setWeapon, bool isStart) {
        IsStart = isStart;
        for (int i = 0; i < setWeapon.Length; ++i) {
            GameObject playerBox = playerBoxes[i];
            if (playerBox.GetActive() == true) {
                Text weaponNameText = playerBox.transform.Find("weaponNameText").GetComponent<Text>();
                weaponNameText.text = _Weapons[setWeapon[i]];
            }
        }
    }

	public void PlayerPanel_toggleUpdate() {
		photonView.RPC ("SyncSuffState", PhotonTargets.All, suffToggle.isOn);
	}

	[PunRPC]
	public void SyncSuffState(bool isSuff) {
		suffToggle.isOn = isSuff;
	}

    [PunRPC]
    private void ShowPlayerList(int playerCount, int[] hashcode, string[] names) {
        for (int i = 0; i < playerCount; ++i) {
            Transform playerBox = playerBoxes[i].transform;
            playerBox.gameObject.SetActive(true);

            Text playerNameText = playerBox.Find("PlayerNameText").GetComponent<Text>();
            playerNameText.text = names[i].Trim();
        }

    }
}
